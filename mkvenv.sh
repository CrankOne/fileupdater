#!/bin/sh

VENVDIR=./.venv/

mkdir $VENVDIR && cd $VENVDIR && virtualenv --no-site-packages --prompt="PY-VENV\n" venv
